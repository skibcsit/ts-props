"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Obj3dSpecCheck = exports.MySpecCheck = void 0;
var fast_check_1 = require("fast-check");
var THREE = require("three");
// class QuickCheckInterpreter implements Algebra<fc.Arbitrary<number>, fc.IPropertyWithHooks<[a: number]>> {
//     forall(x: fc.Arbitrary<number>, y: (a: any) => boolean) {
//         return fc.property(x, (a) => {
//             return y(a)
//         })
//     }
//     equal(a:any,b:any){
//         return a == b
//     }
//     nat = fc.integer()
// }
var QuickCheckInterpreter = /** @class */ (function () {
    function QuickCheckInterpreter() {
    }
    QuickCheckInterpreter.prototype.forall = function (x, y) {
        return fast_check_1.default.property(x, function (a) {
            return y(a);
        });
    };
    QuickCheckInterpreter.prototype.equal = function (a, b) {
        return a == b;
    };
    return QuickCheckInterpreter;
}());
//Property изменён на boolean
var MySpec = /** @class */ (function () {
    function MySpec(alg, nat) {
        this.alg = alg;
        this.nat = fast_check_1.default.integer();
    }
    MySpec.prototype.succ = function (x) {
        return x + 1;
    };
    MySpec.prototype.pred = function (x) {
        return x - 1;
    };
    MySpec.prototype.predsuccprop = function () {
        var _this = this;
        return this.alg.forall(this.nat, function (x) {
            return _this.alg.equal(_this.pred(_this.succ(x)), x);
        });
    };
    return MySpec;
}());
var MySpecCheck = new MySpec(new QuickCheckInterpreter, fast_check_1.default.integer());
exports.MySpecCheck = MySpecCheck;
var Obj3dSpec = /** @class */ (function () {
    function Obj3dSpec(alg, nat) {
        this.alg = alg;
        this.nat = nat;
    }
    Obj3dSpec.prototype.succ = function (x) {
        return x + 1;
    };
    Obj3dSpec.prototype.pred = function (x) {
        return x - 1;
    };
    Obj3dSpec.prototype.selfequalprop = function () {
        return this.alg.forall(this.nat, function (obj) {
            return obj instanceof THREE.Object3D;
        });
    };
    return Obj3dSpec;
}());
var vector3Arbitrary = function () {
    return fast_check_1.default.record({
        x: fast_check_1.default.float({ min: -1000, max: 1000 }),
        y: fast_check_1.default.float({ min: -1000, max: 1000 }),
        z: fast_check_1.default.float({ min: -1000, max: 1000 })
    });
};
var quaternionArbitrary = function () {
    return fast_check_1.default.record({
        x: fast_check_1.default.float({ min: -1, max: 1 }),
        y: fast_check_1.default.float({ min: -1, max: 1 }),
        z: fast_check_1.default.float({ min: -1, max: 1 }),
        w: fast_check_1.default.float({ min: -1, max: 1 })
    });
};
var object3DArbitrary = function () {
    return fast_check_1.default.record({
        position: vector3Arbitrary(),
        rotation: vector3Arbitrary(),
        scale: vector3Arbitrary(),
        quaternion: quaternionArbitrary(),
        matrix: fast_check_1.default.array(fast_check_1.default.float(), { minLength: 16, maxLength: 16 }),
        matrixWorld: fast_check_1.default.array(fast_check_1.default.float(), { minLength: 16, maxLength: 16 }),
        matrixAutoUpdate: fast_check_1.default.boolean(),
        matrixWorldNeedsUpdate: fast_check_1.default.boolean(),
        visible: fast_check_1.default.boolean(),
        castShadow: fast_check_1.default.boolean(),
        receiveShadow: fast_check_1.default.boolean(),
        frustumCulled: fast_check_1.default.boolean(),
        renderOrder: fast_check_1.default.integer()
    });
};
var generateRandomObject3D = function () {
    return object3DArbitrary().map(function (props) {
        var obj = new THREE.Object3D();
        obj.position.set(props.position.x, props.position.y, props.position.z);
        obj.rotation.set(props.rotation.x, props.rotation.y, props.rotation.z);
        obj.scale.set(props.scale.x, props.scale.y, props.scale.z);
        obj.quaternion.set(props.quaternion.x, props.quaternion.y, props.quaternion.z, props.quaternion.w);
        obj.matrix.fromArray(props.matrix);
        obj.matrixWorld.fromArray(props.matrixWorld);
        obj.matrixAutoUpdate = props.matrixAutoUpdate;
        obj.matrixWorldNeedsUpdate = props.matrixWorldNeedsUpdate;
        obj.visible = props.visible;
        obj.castShadow = props.castShadow;
        obj.receiveShadow = props.receiveShadow;
        obj.frustumCulled = props.frustumCulled;
        obj.renderOrder = props.renderOrder;
        return obj;
    });
};
// class MatrixSpec<Set, Prop> {
//     alg: Algebra<Set, Prop>
//     e = new THREE.Euler(0, 0, 1);
//     obj3d = new THREE.Object3D;
//     previousRotation = this.obj3d.rotation;
//     constructor(alg: Algebra<Set, Prop>) {
//         this.alg = alg;
//     }
//     predsuccprop(): Prop {
//         return this.alg.forall(this.alg.nat, (x, y, z) => {
//             this.obj3d.setRotationFromEuler(new THREE.Euler(x, y, z))
//             return this.obj3d.rotation == this.previousRotation + new THREE.Euler(x, y, z)
//         })
//     }
// }
var Obj3dSpecCheck = new Obj3dSpec(new QuickCheckInterpreter, generateRandomObject3D());
exports.Obj3dSpecCheck = Obj3dSpecCheck;
var PrintInterpreter = /** @class */ (function () {
    function PrintInterpreter() {
        this.nat = "fc.integer()";
    }
    PrintInterpreter.prototype.forall = function (x, y) {
        console.log("print specification");
        return ("print specification");
    };
    PrintInterpreter.prototype.equal = function (a, b) {
        return a == b;
    };
    return PrintInterpreter;
}());
