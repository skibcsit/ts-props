import fc from 'fast-check';
import * as THREE from 'three';
interface Algebra<Set, Prop> {
    forall(x: Set, y: (a: any) => boolean): Prop
    equal(a:any,b:any):boolean
}

// class QuickCheckInterpreter implements Algebra<fc.Arbitrary<number>, fc.IPropertyWithHooks<[a: number]>> {
//     forall(x: fc.Arbitrary<number>, y: (a: any) => boolean) {
//         return fc.property(x, (a) => {
//             return y(a)
//         })
//     }
//     equal(a:any,b:any){
//         return a == b
//     }
//     nat = fc.integer()
// }

class QuickCheckInterpreter<P> implements Algebra<fc.Arbitrary<P>, fc.IPropertyWithHooks<[a: P]>> {
    forall(x: fc.Arbitrary<P>, y: (a: any) => boolean) {
        return fc.property(x, (a) => {
            return y(a)
        })
    }
    equal(a:any,b:any){
        return a == b
    }
}




//Property изменён на boolean

class MySpec<Set, Prop> {
    alg: Algebra<fc.Arbitrary<number>, Prop>
    nat: fc.Arbitrary<number>
    constructor(alg: Algebra< fc.Arbitrary<number>, Prop>, nat:fc.Arbitrary<number>) {
        this.alg = alg;
        this.nat = fc.integer()
    }
    succ(x: number) {
        return x + 1
    }
    pred(x: number) {
        return x - 1
    }
    predsuccprop(): Prop {
        return this.alg.forall(this.nat, (x) => {
            return this.alg.equal(this.pred(this.succ(x)),x)
        })

    }
}

const MySpecCheck = new MySpec(new QuickCheckInterpreter, fc.integer())
export { MySpecCheck }

class Obj3dSpec<Set, Prop> {
    alg: Algebra<fc.Arbitrary<THREE.Object3D<THREE.Object3DEventMap>>, Prop>
    nat: fc.Arbitrary<THREE.Object3D<THREE.Object3DEventMap>>
    constructor(alg: Algebra<fc.Arbitrary<THREE.Object3D<THREE.Object3DEventMap>>, Prop>, nat:fc.Arbitrary<THREE.Object3D<THREE.Object3DEventMap>>) {
        this.alg = alg;
        this.nat = nat;
    }
    succ(x: number) {
        return x + 1
    }
    pred(x: number) {
        return x - 1
    }
    selfequalprop(): Prop {
        return this.alg.forall(this.nat,(obj)=>{
            return obj instanceof THREE.Object3D
        })
    }
}




const vector3Arbitrary = () =>
  fc.record({
    x: fc.float({min:-1000, max:1000}),
    y: fc.float({min:-1000, max:1000}),
    z: fc.float({min:-1000, max:1000})
  });

const quaternionArbitrary = () =>
fc.record({
x: fc.float({min:-1, max:1}),
y: fc.float({min:-1, max:1}),
z: fc.float({min:-1, max:1}),
w: fc.float({min:-1, max:1})
});  

const object3DArbitrary = () =>
  fc.record({
    position: vector3Arbitrary(),
    rotation: vector3Arbitrary(),
    scale: vector3Arbitrary(),
    quaternion: quaternionArbitrary(),
    matrix: fc.array(fc.float(), { minLength: 16, maxLength: 16 }),
    matrixWorld: fc.array(fc.float(), { minLength: 16, maxLength: 16 }),
    matrixAutoUpdate: fc.boolean(),
    matrixWorldNeedsUpdate: fc.boolean(),
    visible: fc.boolean(),
    castShadow: fc.boolean(),
    receiveShadow: fc.boolean(),
    frustumCulled: fc.boolean(),
    renderOrder: fc.integer()
  });


  const generateRandomObject3D = () =>
  object3DArbitrary().map(props => {
    const obj = new THREE.Object3D();
    obj.position.set(props.position.x, props.position.y, props.position.z);
    obj.rotation.set(props.rotation.x, props.rotation.y, props.rotation.z);
    obj.scale.set(props.scale.x, props.scale.y, props.scale.z);
    obj.quaternion.set(props.quaternion.x, props.quaternion.y, props.quaternion.z, props.quaternion.w);
    obj.matrix.fromArray(props.matrix);
    obj.matrixWorld.fromArray(props.matrixWorld);
    obj.matrixAutoUpdate = props.matrixAutoUpdate;
    obj.matrixWorldNeedsUpdate = props.matrixWorldNeedsUpdate;
    obj.visible = props.visible;
    obj.castShadow = props.castShadow;
    obj.receiveShadow = props.receiveShadow;
    obj.frustumCulled = props.frustumCulled;
    obj.renderOrder = props.renderOrder;
    return obj;
  });




// class MatrixSpec<Set, Prop> {
//     alg: Algebra<Set, Prop>
//     e = new THREE.Euler(0, 0, 1);
//     obj3d = new THREE.Object3D;
//     previousRotation = this.obj3d.rotation;

//     constructor(alg: Algebra<Set, Prop>) {
//         this.alg = alg;
//     }
//     predsuccprop(): Prop {
//         return this.alg.forall(this.alg.nat, (x, y, z) => {
//             this.obj3d.setRotationFromEuler(new THREE.Euler(x, y, z))
//             return this.obj3d.rotation == this.previousRotation + new THREE.Euler(x, y, z)
//         })

//     }
// }

const Obj3dSpecCheck = new Obj3dSpec(new QuickCheckInterpreter, generateRandomObject3D())
export { Obj3dSpecCheck }


class PrintInterpreter implements Algebra<string, string> {
    forall(x: string, y: (a: any) => boolean) {
        console.log("print specification")
        return ("print specification")
    }
    equal(a:any,b:any){
        return a == b
    }
    nat = "fc.integer()"

}